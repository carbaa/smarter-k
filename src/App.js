import React from "react";

import GradeDisplay from "./components/GradeDisplay";
import FrequencyDataSelector from "./components/FrequencyDataSelector";
import KanjiDisplay from "./components/KanjiDisplay";

import kyoiku from "./data/kyoiku.json";

import news from "./data/converted_news.json";
import aozora from "./data/converted_aozora.json";
import twitter from "./data/converted_twitter.json";
import wikipedia from "./data/converted_wikipedia.json";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      grade: 1,
      selectedDataName: "news"
    };

    this.frequencyData = { news, aozora, twitter, wikipedia };
  }

  updateGrade(grade) {
    this.setState({ grade });
  }

  updateSelectedDataName(selectedDataName) {
    this.setState({ selectedDataName });
  }

  render() {
    const selectedDataName = this.state.selectedDataName;
    let selectedData = this.frequencyData[selectedDataName];

    kyoiku[this.state.grade].sort(
      (a, b) => selectedData[b].count - selectedData[a].count
    );

    return (
      <div>
        <header>
          <h1 class="display-3">Smarter K</h1>
        </header>
        <nav className="control">
          <GradeDisplay
            grade={this.state.grade}
            updateGrade={this.updateGrade.bind(this)}
          />
          <FrequencyDataSelector
            dataNames={Object.keys(this.frequencyData)}
            selectedDataName={this.selectedDataName}
            updateSelectedDataName={this.updateSelectedDataName.bind(this)}
          />
        </nav>
        <br />
        <div className="grid">
          {kyoiku[this.state.grade].map(input => {
            let count = selectedData[input].count;
            let freq = selectedData[input].freq;
            return <KanjiDisplay kanji={input} count={count} freq={freq} />;
          })}
        </div>
        <br />
        <footer>
          This app uses kanji frequency data from{" "}
          <a href="https://github.com/scriptin/kanji-frequency">
            Dmitry Shpika
          </a>{" "}
          licensed under{" "}
          <a href="https://creativecommons.org/licenses/by/4.0/legalcode">
            CC BY 4.0
          </a>
        </footer>
      </div>
    );
  }
}

export default App;

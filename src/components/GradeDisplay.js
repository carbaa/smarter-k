import React from "react";

class GradeDisplay extends React.Component {
  onSubtract() {
    this.props.updateGrade(this.props.grade - 1);
  }

  onAdd() {
    this.props.updateGrade(this.props.grade + 1);
  }

  render() {
    let enableLeft = this.props.grade !== 1;
    let enableRight = this.props.grade !== 6;

    return (
      <div className="grade-display">
        {enableLeft ? (
          <button className="btn btn-dark" onClick={this.onSubtract.bind(this)}>
            {" "}
            &#060;{" "}
          </button>
        ) : null}
        <span className="lead">Current Grade : {this.props.grade}</span>
        {enableRight ? (
          <button className="btn btn-dark" onClick={this.onAdd.bind(this)}>
            {" "}
            &#062;{" "}
          </button>
        ) : null}
      </div>
    );
  }
}

export default GradeDisplay;

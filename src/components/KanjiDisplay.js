import React from "react";
import KanjiStatsDisplay from "./KanjiStatsDisplay";

class KanjiDisplay extends React.Component {
  render() {
    return (
      <div className="kanji">
        <KanjiStatsDisplay count={this.props.count} freq={this.props.freq} />
        {this.props.kanji}
      </div>
    );
  }
}

export default KanjiDisplay;

import React from "react";

class FrequencyDataSelector extends React.Component {
  onChange(e) {
    this.props.updateSelectedDataName(e.target.value);
  }

  render() {
    return (
      <div className="data-selector">
        <select
          className="custom-select"
          value={this.props.selectedDataName}
          onChange={this.onChange.bind(this)}
        >
          {this.props.dataNames.map(name => (
            <option value={name}>{name}</option>
          ))}
        </select>
      </div>
    );
  }
}

export default FrequencyDataSelector;

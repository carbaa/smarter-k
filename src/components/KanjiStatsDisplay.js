import React from "react";

function KanjiStatsDisplay(props) {
  let freqToPercent = parseFloat(props.freq) * 100;

  return (
    <div className="kanji-stats-bubble">
      Count: {props.count}
      <br />
      Frequency:{" "}
      {freqToPercent < 0.01 ? "Less than 0.01" : freqToPercent.toFixed(3)}%
    </div>
  );
}

export default KanjiStatsDisplay;

import json
import sys
import os

if sys.argv:
    for filepath in sys.argv[1:]:
        try:
            f = open(filepath, encoding="utf-8")
        except IOError:
            print(f"Couldn't open {filepath}")
            continue

        try:
            scriptin_json = json.load(f)
        except:
            print("Invalid JSON")
            continue

        dic = {
            kanji: {"count": count, "freq": freq}
            for kanji, count, freq in scriptin_json
        }

        try:
            _, tail = os.path.split(filepath)
            root, _ = os.path.splitext(tail)
            new_filename = f"converted_{root}.json"

            f = open(new_filename, "w", encoding="utf-8")
        except IOError:
            print(f"Couldn't write to {new_filename}")
            continue

        try:
            json.dump(dic, f)
        except:
            print(f"Error writing {new_filename}")
            continue


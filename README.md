# Smarter K

An educational tool that shows [Kyoiku kanji](https://en.wikipedia.org/wiki/Ky%C5%8Diku_kanji) sorted by frequency based on a selection of corpuses.

You can see the live demo at <https://carbaa.gitlab.io/smarter-k/>

## Getting Started

- `npm install` to install dependencies
- `npm start` to run this on a development server

## Included Data

This project uses kanji frequency data from [Dmitry Shpika](https://github.com/scriptin/kanji-frequency) licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)

The data have been converted from their original JSON format for ease of access in this project. The script used to convert the data can be found in the `util` folder.
